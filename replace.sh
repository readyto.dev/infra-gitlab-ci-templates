#!/bin/bash


if [ -z "$1" ]
  then
    echo "Provide an environment name"
    exit 1
fi

ENV=$1

SED_PATTERN=$(cat .$1.env | sed 's/=/}}|/g' | sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/|g; s|{{/g' )

rm -rf output || true
cp -rp src output

find output -type f -exec sed -i "s|{{$SED_PATTERN|g" {} \;
